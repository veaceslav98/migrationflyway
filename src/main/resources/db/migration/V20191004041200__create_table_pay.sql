CREATE TABLE HOMEWORK.pay
    (
        cardNr NUMBER(3) NOT NULL,
        salary NUMBER(8,2),
        commission_pct NUMBER(2,2),
        PRIMARY KEY(cardNr)
    );