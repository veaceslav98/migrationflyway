CREATE TABLE employment_logs (

	employment_log_id NUMBER(4) default 0,
	first_name VARCHAR2(25),
	last_name VARCHAR2(25),
	employment_action VARCHAR2(6),
	employment_status_updtd_tmstmp VARCHAR2(25),
    CONSTRAINT employee_action_variants CHECK (employment_action = 'hired' or employment_action = 'fired')
);