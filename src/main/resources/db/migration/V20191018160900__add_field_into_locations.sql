ALTER TABLE LOCATIONS ADD department_amount NUMBER(4) default 0;

COMMENT ON COLUMN locations.department_amount IS 'Contains the amount of departments in the location';