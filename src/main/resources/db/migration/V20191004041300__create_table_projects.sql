CREATE TABLE HOMEWORK.projects
    (
        project_id NUMBER(4) PRIMARY KEY ,
        project_description VARCHAR2(20),
        project_investments NUMBER(7,-3) DEFAULT 0,
        project_revenue NUMBER(7),
        project_time NUMBER(7),
        CONSTRAINT project_description_length CHECK (LENGTH(project_description) > 10)
    );