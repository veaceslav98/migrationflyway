CREATE OR REPLACE TRIGGER change_employment_logs
    AFTER INSERT OR DELETE
    ON EMPLOYEES FOR EACH ROW
BEGIN
    CASE
    WHEN INSERTING THEN
        INSERT INTO EMPLOYMENT_LOGS VALUES (3, :NEW.first_name, :NEW.last_name, 'hired', 'updated');
    WHEN DELETING THEN
        INSERT INTO EMPLOYMENT_LOGS VALUES (4, :OLD.first_name, :OLD.last_name, 'fired', 'updated');
    END CASE;
END;